const { expect } = require("chai");
const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());
const {collectionValues} = require('../test_data/TestContract.data');

const { abi, evm } = require("../compile");

let accounts;
let contract;
let accountBalance;

beforeEach(async () => {
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts();

  //Account Balance Check
  web3.eth.getBalance(accounts[0], function (err, result) {
    if (err) {
      console.log(err);
    } else {
      accountBalance = web3.utils.fromWei(result, "ether");
      global.accountBalance = accountBalance;
    }
  })

  contract = await new web3.eth.Contract(abi)
    .deploy({
      data: evm.bytecode.object,
      arguments: ["Hi there!"],
    })
    .send({ from: accounts[0], gas: "1000000" });

  global.accounts = accounts;
  global.contract = contract;
  console.log("----> before each");
});

describe("Test Contract", async () => {

  let collectionsOfData = [];

  it("deploys a contract", () => {

    assert.ok(contract.options.address);
  });

  it("has a default message", async () => {

    const message = await contract.methods.message().call();
    assert.equal(message, "Hi there!");
  });

  it("Insert collections of data to struct", async () => {

    for (let cv of collectionValues) {

      await contract.methods
        .addCollection(cv.id, "" + cv.name + "")
        .send({ from: accounts[0] });
    }

    collectionsOfData = await contract.methods.getCollection().call();
    console.log("collectionsOfData : " + collectionsOfData);

    //data verification
    for (i in collectionsOfData) {

      assert.equal(collectionsOfData[i].name, collectionValues[i].name);
      expect(collectionsOfData[i].id).to.equal("" + collectionValues[i].id + "");
      console.log(collectionsOfData[i].name + "=" + collectionValues[i].name);
      console.log(collectionsOfData[i].id + "=" + collectionValues[i].id);
    }
  }).timeout(10000);

  it("Insert data to struct", async () => {

    await contract.methods
      .addCollection(1, "Biswanath")
      .send({ from: accounts[0] });

    let collections = await contract.methods.getCollection().call();
    console.log("collections---->" + collections);
    // mocha
    assert.equal(collections[0].name, "Biswanath");
    // chai
    expect(collections[0].id).to.equal("1");
    // console.log("----->", collections);
  });
});