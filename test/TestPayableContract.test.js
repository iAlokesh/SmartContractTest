const { expect } = require("chai");
const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());
const { runs,ethers } = require('../test_data/TestPayableContract.data');

const { abi, evm } = require("../compile");

describe("Test Payable Contract", async () => {

    runs.forEach((run) => {
        it("can change the message If required amount gets spent " + run.it, async () => {

            await contract.methods.setMessage(run.it).send({ from: accounts[0], value: web3.utils.toWei('0.010', 'ether') });
            const message = await contract.methods.message().call();
            console.log("Message : " + message);

            try {
                assert.equal(message, run.it);
            } catch (err) {
                console.log("Error : ----->" + err.message);
            }
        });
    });

    ethers.forEach((ether) => {
        it("Error on not spending required amount to set message " + ether.it, async () => {

            let errormessage;
            try {
                await contract.methods.setMessage("Abc").send({ from: accounts[0], value: web3.utils.toWei('' + ether.it + '', 'ether') })
            } catch (err) {
                errormessage = err.message;
                //console.log("Error :-->"+err.message);
            }
            assert.equal(errormessage, "VM Exception while processing transaction: revert");
        });
    });
});