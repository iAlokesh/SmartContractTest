const { expect } = require("chai");
const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());

const { abi, evm } = require("../compile");

describe("Test Contract Fees", async () => {

  let currentAccountBalance;

  it("Current account balance check", () => {

    console.log("accountBalance : " + accountBalance);
    currentAccountBalance = accountBalance;
  });

  it("Spent amount for a transaction should be more than 0.001 ether", () => {

    let spentAmount;
    spentAmount = currentAccountBalance - accountBalance;

    //Condition check
    expect(spentAmount).to.greaterThan(0.001);
  });
});