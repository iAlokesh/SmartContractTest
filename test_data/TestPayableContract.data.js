let runs = [
    { it: 'Hello' },
    { it: 'Tree' },
    { it: 'Smart Contract' },
    { it: 'WebDriverIO' },
    { it: 'Blockchain' },
    { it: 'Cryptocurrency' },
    { it: 'Azure' },
    { it: 'Solidity' },
    { it: 'Java Script' },
    { it: 'Chai Mocha' }
];

let ethers = [
    { it: 0.009 },
    { it: 0.0085 },
    { it: 0.0095 },
    { it: 0.0099 },
    { it: 0.00999 },
    { it: 0.00998 },
    { it: 0.00997 },
    { it: 0.009996 },
    { it: 0.009993 },
    { it: 0.009999 },
];

module.exports={runs,ethers};