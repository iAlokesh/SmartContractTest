let collectionValues = [
    { id: 1, name: 'Smart Contract' },
    { id: 2, name: 'Azure' },
    { id: 3, name: 'Chai-Mocha' },
    { id: 4, name: 'Blockchain' },
    { id: 5, name: 'Cryptocurrency' },
    { id: 6, name: 'Blockchain' },
    { id: 7, name: 'JavaScript' },
    { id: 8, name: 'NodeJS' },
    { id: 9, name: 'VS Code' },
    { id: 10, name: 'WebDriver IO' }
  ];

module.exports={collectionValues};  